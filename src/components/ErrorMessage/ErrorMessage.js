import React from "react";

import styled from "styled-components";
import cat from "../../images/cat.png";

const StyledErrorMessage = styled.div`
  height: 250px;
  margin-left: 30%;
  img {
    width: 150px;
    margin-left: 90px;
  }
  h5 {
    margin-left: 30px;
    margin-top: 15px;
  }
`;

export const ErrorMessage = () => {
  return (
    <StyledErrorMessage className="text-warning mt-3">
      <img src={cat} alt="cat" />
      <h5>something has gone wrong</h5>
      <h2>PLEASE RELOAD PAGE</h2>
    </StyledErrorMessage>
  );
};
