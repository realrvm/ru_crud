import React from "react";

export const GetNewUser = ({ getNewUserButton }) => {
  const handleClick = () => {
    getNewUserButton();
  };
  return (
    <button
      type="button"
      className="btn btn-outline-warning mb-5"
      onClick={handleClick}
    >
      GET NEW RANDOM USER
    </button>
  );
};
