import React from "react";

import styled from "styled-components";

const SpinnerDiv = styled.div`
  height: 250px;

  .spinn {
    width: 120px;
    height: 120px;
    margin-left: 42%;
  }
`;

export const Spinner = () => {
  return (
    <SpinnerDiv>
      <div className="spinner-border text-warning spinn" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    </SpinnerDiv>
  );
};
