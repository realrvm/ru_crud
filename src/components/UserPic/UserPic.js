import React from "react";

import styled from "styled-components";

const StyledImg = styled.div`
  img {
    width: 250px;
    border: 3px solid white;
    border-radius: 5px;
  }
`;

export const UserPic = (props) => {
  return (
    <StyledImg>
      <img src={props.children} alt="pic" />
    </StyledImg>
  );
};
