import React from "react";

export class BreakApp extends React.Component {
  state = {
    renderError: false,
  };

  render() {
    if (this.state.renderError) {
      this.wonderland.alice = 0;
    }
    return (
      <button
        className="  btn btn-outline-danger mb-5 ml-5"
        onClick={() => this.setState({ renderError: true })}
      >
        BREAK THE APP
      </button>
    );
  }
}
