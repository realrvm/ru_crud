import React from "react";

export const User = ({ userName }) => {
  const { firstName, lastName } = userName;
  return (
    <div className="text-white ml-5 mt-5">
      <h5>Hi, my name is</h5>
      <h1>
        {firstName} {lastName}
      </h1>
    </div>
  );
};
