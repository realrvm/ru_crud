import React from "react";
import { Route } from "react-router-dom";

import styled from "styled-components";

import UserPic from "../../components/UserPic";
import User from "../User/";
import Email from "../Email";
import Age from "../Age";
import Location from "../Location";
import Cell from "../Cell";
import Password from "../Password";

const StyledIntro = styled.div`
  height: 250px;
`;

export const Intro = ({ userInfo }) => {
  const { picture, ...userName } = userInfo;
  return (
    <StyledIntro className="d-flex justify-content-between mr-5 ">
      <Route exact path="/" render={() => <User userName={userName} />} />
      <Route path="/email" render={() => <Email userName={userName} />} />
      <Route path="/age" render={() => <Age userName={userName} />} />
      <Route path="/location" render={() => <Location userName={userName} />} />
      <Route path="/cell" render={() => <Cell userName={userName} />} />
      <Route path="/password" render={() => <Password userName={userName} />} />

      {/* <UserPic picture={picture} /> */}
      <UserPic>{picture}</UserPic>
    </StyledIntro>
  ); // передача свойств дочернему компоненту  с помощью props.children
};
