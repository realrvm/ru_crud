import React from "react";

export const Age = ({ userName }) => {
  const { age } = userName;
  return (
    <div className="text-white ml-5 mt-5">
      <h5> My age is</h5>
      <h4>{age}</h4>
    </div>
  );
};
