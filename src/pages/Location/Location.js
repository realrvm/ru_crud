import React from "react";

export const Location = ({ userName }) => {
  const { location } = userName;
  return (
    <div className="text-white ml-5 mt-5">
      <h5> My location is</h5>
      <h4>{location}</h4>
    </div>
  );
};
