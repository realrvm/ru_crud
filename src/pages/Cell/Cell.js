import React from "react";

export const Cell = ({ userName }) => {
  const { cell } = userName;
  return (
    <div className="text-white ml-5 mt-5">
      <h5> My cell is</h5>
      <h4>{cell}</h4>
    </div>
  );
};
