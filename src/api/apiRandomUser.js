export class RandomUser {
  _baseURL = "https://randomuser.me/api/";

  getUser = async (url) => {
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error(
        `Could not fetch ${url} , because receive ${response.status}`
      );
    }
    return await response.json();
  };

  getRandomUser = async () => {
    const user = await this.getUser(
      `${this._baseURL}?inc=name, email, dob, location, cell,  login, picture , gender`
    );
    return this._transformUser(user);
  };

  _transformUser = (user) => {
    const [result] = user.results;
    const { name, email, dob, location, cell, login, picture, gender } = result;
    return {
      gender,
      name,
      email,
      dob,
      location,
      cell,
      login,
      picture,
    };
  };
}
