import React from "react";
import { BrowserRouter as Router } from "react-router-dom";

import styled from "styled-components";

import { RandomUser } from "./api/apiRandomUser";
import Spinner from "./components/Spinner";
import ErrorMessage from "./components/ErrorMessage";
import GetNewUser from "./components/GetNewUser";
import BreakApp from "./components/BreakApp";

import NavBar from "./components/NavBar/";
import Intro from "./pages/Intro";

const StyledApp = styled.div`
  width: 900px;
  margin: 0 auto;
  border-radius: 10px;
  .intro {
    height: 340px;
  }
`;

class App extends React.Component {
  api = new RandomUser();

  state = {
    hasError: false,
    button: false,
    loading: true,
    error: false,
    picture: null,
    firstName: null,
    lastName: null,
    gender: null,
  };

  componentDidMount() {
    this.updateUser();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.button !== this.state.button) {
      this.updateUser();
    }
  }

  componentDidCatch() {
    this.setState({ hasError: true });
  }

  onError = (error) => {
    this.setState({
      error: true,
      loading: false,
    });
  };

  onUserLoaded = (user) => {
    this.setState({
      loading: false,
      picture: user.picture.large,
      firstName: user.name.first,
      lastName: user.name.last,
      email: user.email,
      age: user.dob.age,
      location: user.location.city,
      cell: user.cell,
      password: user.login.password,
    });
  };

  updateUser() {
    this.api.getRandomUser().then(this.onUserLoaded).catch(this.onError);
  }

  getNewUserButton = () => {
    this.setState((prevState) => {
      return { ...prevState, button: !prevState.button };
    });
  };

  render() {
    if (this.state.hasError) {
      return <ErrorMessage />;
    }
    const { loading, error, ...userInfo } = this.state;

    const hasContent = !(loading || error);

    const errorMessage = error ? <ErrorMessage /> : null;
    const spinner = loading ? <Spinner /> : null;

    const intro = hasContent ? <Intro userInfo={userInfo} /> : null;

    return (
      <Router>
        <StyledApp className="jumbotron bg-dark mt-3">
          <div className="container">
            {errorMessage}
            {spinner}
            {intro}
            <div className="d-flex">
              <GetNewUser getNewUserButton={this.getNewUserButton} />
              <BreakApp />
            </div>
            <NavBar />
          </div>
        </StyledApp>
      </Router>
    );
  }
}
export default App;
